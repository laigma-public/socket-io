<img src="./assets/lp-logo.png" alt="Ligma Prods" height="60"/>
<img src="https://socket.io/images/logo.svg" alt="Socket IO" height="60"/>

# Socket IO Server

## Requisitos
- node 14.x o superior
- npm


## Instrucciones

### 1- Instalar dependencias
```
npm install
```

### 2- Definir .env.development y .env.production
Crea los archivos para variables de entorno desarrollo y producción:
- .env.development
```
NODE_ENV=development
IO_HOST=localhost
IO_PORT=3000
```
- .env.production
```
NODE_ENV=production
IO_HOST=<host de produccion>
IO_PORT=3000
```
   
### 3- Ejecutar localmente
- Desarrollo
```
npm run dev
```
- Producción
```
npm run start
```

### 4- Despliegue en producción (Requiere docker & docker-compose)
```
docker-compose up --build -d
```


## Enlaces de interés
- https://socket.io/