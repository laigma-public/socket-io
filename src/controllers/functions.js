import { io } from "../index.js";

export default {
  ioMessage(args, res) {
    res.setHeader("Content-Type", "application/json");
    io.emit(args.channel.value, args.message.value);
    res.end(JSON.stringify({ status: "Mensaje enviado a Socket-IO" }));
  },

  postSocket(args, res) {
    res.setHeader("Content-Type", "application/json");
    io.emit(args.body.value.channel, args.body.value.message);
    res.end(JSON.stringify({ status: "Mensaje enviado a Socket IO" }));
  },
}